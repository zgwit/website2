---
title: "Count方法"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-04"
weight: 6500
toc: true
---

### Count方法

统计数据使用`Count`方法，Count方法的参数为struct的指针并且成为查询条件。
```Go
user := new(User)
total, err := engine.Where("id >?", 1).Count(user)
```
