---
title: "创建索引和唯一索引"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-03"
weight: 8300
toc: true
---

## 创建索引和唯一索引

* CreateIndexes

根据struct中的tag来创建索引

* CreateUniques

根据struct中的tag来创建唯一索引
