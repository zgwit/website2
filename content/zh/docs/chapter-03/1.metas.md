---
title: "获取数据库信息"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-03"
weight: 8100
toc: true
---

## 表结构操作

xorm 提供了一些动态获取和修改表结构的方法，通过这些方法可以动态同步数据库结构，导出数据库结构，导入数据库结构。

## 获取数据库信息

* DBMetas()

xorm支持获取表结构信息，通过调用 `engine.DBMetas()` 可以获取到数据库中所有的表，字段，索引的信息。

* TableInfo()

根据传入的结构体指针及其对应的Tag，提取出模型对应的表结构信息。这里不是数据库当前的表结构信息，而是我们通过struct建模时希望数据库的表的结构信息
