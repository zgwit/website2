---
title: "Database schema operation"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-03"
weight: 8100
toc: true
---

# 3. database meta information

xorm provides methods to getting and setting table schema. For less schema changing production, `engine.Sync2()` is enough.

## 3.1 retrieve database meta info

* DBMetas()

`engine.DBMetas()` returns all tables schema information.

## 3.2.directly table operation

* CreateTables()

`engine.CreateTables(struct)` creates table with struct or struct pointer.
`engine.Charset()` and `engine.StoreEngine()` can change charset or storage engine for **mysql** database.

* IsTableEmpty()

check table is empty or not.

* IsTableExist()

check table is existed or not.

* DropTables()

`engine.DropTables(struct)` drops table and indexes with struct or struct pointer. `engine.DropTables(string)` only drops table except indexes.

## 3.3.create indexes and uniques

* CreateIndexes
create indexes with struct.

* CreateUniques
create unique indexes with struct.

## 3.4.Synchronize database schema

xorm watches tables and indexes and sync schema:

1) use table name to create or drop table
2) use column name to alter column
3) use the indexes definition in struct field tag to create or drop indexes.

```Go
err := engine.Sync(new(User))
```
